export interface Option {
    id: number;
    name: string;
}
  
export interface Contact {
    name: string;
    company_name: string;
    email: string;
    phone: string;
    message: string;
    category: number | string
}