import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private SERVER_URL = "http://localhost:3000";

  constructor(private httpClient: HttpClient) { }

  public createContact(data: any){  
		return this.httpClient.post(`${this.SERVER_URL}/contacts/`, data);  
	}  

}
