import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private SERVER_URL = "http://localhost:3000";

  constructor(private httpClient: HttpClient) { }

  
	public getCategories(){  
		return this.httpClient.get(`${this.SERVER_URL}/contacts/categories`);  
	}  

}
