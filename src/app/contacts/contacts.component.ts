import { Component, OnInit } from '@angular/core';
import { Contact, Option } from '../../interfaces/models';
import { CategoryService } from '../services/category.service';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  header = 'Contacto de soporte!';  
 
  public categories: Option[];
  public contact: Contact;
  public contacts: Contact[];
  public errors: any; 
  public isSaved: boolean; 

  constructor(
    private categoryService: CategoryService,
    private contactService: ContactService
  ) { }

  ngOnInit(): void {
    

    this.clearForm()

    this.isSaved = false

    this.contacts = []

    this.categoryService.getCategories().subscribe((data: any)=>{  
      console.log(data);  
      this.categories = []
      if(data['ok'])

        for (let i = 0; i < data['categories'].length; i++) {
          this.categories.push({
            name: data['categories'][i]['name'],
            id: data['categories'][i]['_id']
          })

          
        }
		})  


  }

  saveContact() {
    // this.contacts.push({ ...this.contact });
    // this.updateCounter();

    this.contactService.createContact(this.contact).subscribe(res => {
      this.isSaved = true

      this.clearForm()
    }, err => {
      if (err.status == 400){
        this.errors = err.error.errors 
      }else {
        alert('Ocurrio un error hable con el administrador.')
      }
    })

  }
  

  private clearForm() {
    this.contact = {
      name: '',
      company_name: '',
      email: '',
      phone: '',
      message: '',
      category: ''
    };

    this.errors = {
      name: {msg: ''},
      company_name: {msg: ''},
      email: {msg: ''},
      phone: {msg: ''},
      message: {msg: ''},
      category: {msg: ''}
    }
  }

}
